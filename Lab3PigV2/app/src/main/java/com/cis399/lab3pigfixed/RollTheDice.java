package com.cis399.lab3pigfixed;


public class RollTheDice {

    public Winner play(int Player1Score, int Player2Score)
    {
        Winner who = Winner.unfinished;
        if (Player1Score >= 20 && Player2Score >= 20)
        {
            if (Player1Score > Player2Score)
            {
                who = Winner.Player1;
            }
            else if (Player1Score < Player2Score)
            {
                who = Winner.Player2;
            }
            else if (Player1Score == Player2Score)
            {
                who = Winner.tie;
            }
        }
        else if (Player1Score >= 20 && Player2Score < 20)
        {
            who = Winner.Player1Lead;
        }
        else if (Player1Score < 20 && Player2Score >= 20)
        {
            who = Winner.Player2Lead;
        }
        return who;

    }


}
