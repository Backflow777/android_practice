package com.cis399.lab3pigfixed;

import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.view.View.OnClickListener;

import java.util.Random;

import android.os.Bundle;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity
implements OnClickListener{

    public static final Random RANDOM = new Random();
    private Button roll;
    private Button reset;
    private ImageView Dice;
    private TextView Score1;
    private TextView Score2;
    private TextView CurrentScore;
    int Player1Score = 0;
    int Player2Score = 0;
    public Turn currentTurn = Turn.Player1;
    RollTheDice game = new RollTheDice();
    Winner who = Winner.unfinished;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        roll = (Button) findViewById(R.id.rollDice);
        reset = (Button) findViewById(R.id.Reset);
        Dice = (ImageView) findViewById(R.id.Die);
        Score1 = (TextView)findViewById(R.id.Score1);
        Score2 = (TextView)findViewById(R.id.Score2);
        CurrentScore = (TextView)findViewById(R.id.ScoreoftheTurn);

        roll.setOnClickListener(this);
        reset.setOnClickListener(this);
    }
    public void GameCalculation() {
        int value = randomDiceValue();
        if (who == Winner.unfinished)
        {
            if (currentTurn == Turn.Player1)
            {
                if (value != 1)
                {
                    Player1Score += value;
                    Score1.setText(Integer.toString(Player1Score));
                    CurrentScore.setText(Integer.toString(value));
                    currentTurn = Turn.Player2;
                }
                else
                {
                    Player1Score = -Player1Score;
                    CurrentScore.setText(Integer.toString(Player1Score));
                    Player1Score = 0;
                    Score1.setText(Integer.toString(Player1Score));
                    currentTurn = Turn.Player2;
                }

            }
            else {
                if (value != 1)
                {
                    Player2Score += value;
                    Score2.setText(Integer.toString(Player2Score));
                    CurrentScore.setText(Integer.toString(value));
                    currentTurn = Turn.Player1;
                }
                else {
                    Player2Score = -Player2Score;
                    CurrentScore.setText(Integer.toString(Player2Score));
                    Player2Score = 0;
                    Score2.setText(Integer.toString(Player2Score));
                    currentTurn = Turn.Player1;
                }

            }
        }
        else if (who == Winner.Player1Lead)
        {
            if (value != 1)
            {
                Player2Score += value;
                Score2.setText(Integer.toString(Player2Score));
                CurrentScore.setText(Integer.toString(value));
            }
            else
            {
                Player2Score = -Player2Score;
                CurrentScore.setText(Integer.toString(Player2Score));
                Player2Score = 0;
                Score2.setText(Integer.toString(Player2Score));
            }
        }
        else if (who == Winner.Player2Lead)
        {
            if (value != 1)
            {
                Player1Score += value;
                Score1.setText(Integer.toString(Player1Score));
                CurrentScore.setText(Integer.toString(value));
            }
            else
            {
                Player1Score = -Player1Score;
                CurrentScore.setText(Integer.toString(Player1Score));
                Player1Score = 0;
                Score1.setText(Integer.toString(Player1Score));
            }
        }
        who = game.play(Player1Score, Player2Score);
        if (who == Winner.Player1) { Score1.setText("PIG"); }
        else if (who == Winner.Player2) { Score2.setText("PIG"); }
        else if (who == Winner.tie)
        {
            Score1.setText("DRAW GAME");
            Score2.setText("DRAW GAME");
        }

        int res1 = getResources().getIdentifier("die" + value, "drawable", "com.cis399.lab3pigfixed");

        Dice.setImageResource(res1);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.rollDice:
                GameCalculation();

            case R.id.Reset:
                Player1Score = 0;
                Player2Score = 0;
                CurrentScore.setText(Integer.toString(Player1Score));
                Score1.setText(Integer.toString(Player1Score));
                Score2.setText(Integer.toString(Player2Score));
                who = Winner.unfinished;
        }
    }

    public static int randomDiceValue() {
        return RANDOM.nextInt(6) + 1;
    }

}